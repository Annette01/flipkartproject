package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class FlpkartSteps   {
	public RemoteWebDriver driver;
	@Given("Load The Browser")
	public void load_The_Browser() {
	    // Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver" ,"./drivers/chromedriver.exe");
		 driver  = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
	}

	@Given("Maximize The Browser")
	public void maximize_The_Browser() {
		
		driver.manage().window().maximize();
	}

	@Given("Close The Popup")
	public void close_The_Popup() {
	    // Write code here that turns the phrase above into concrete actions
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
	}

	@Given("MouseOver The Electronics")
	public void mouseover_The_Electronics() {
		Actions action = new Actions(driver);
		WebElement ele = driver.findElementByXPath("//span[text()='Electronics']");
		action.moveToElement(ele).perform();
	}

	@Given("Click On Mi")
	public void click_On_Mi() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Mi").click();	
	}

	@Then("Verify Title")
	public void verify_Title() {
	    // Write code here that turns the phrase above into concrete actions
		 WebDriverWait wait = new WebDriverWait(driver,10);
			Boolean boolean1 = wait.until(ExpectedConditions.titleContains("Mi"));
					if(boolean1==true)
					{
						System.out.println("Mi is present ");
					}
					else
					{
						System.out.println("Mi is not present ");
					}
	}

}
